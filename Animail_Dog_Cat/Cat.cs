﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animail_Dog_Cat
{
    public class Cat : Animal
    {
        private string _furColor;

        public Cat(string name, int age, string type, string furColor) : base(name, age, type)
        {
            Name = name;
            Age = age;
            Type = type;
            _furColor = furColor;
        }

        public string FurColor
        {
            get => _furColor;
            set => _furColor = value;
        }

        public static void Meow()
        {
            Console.WriteLine("Meow ! Meow !");
        }
    }
}
