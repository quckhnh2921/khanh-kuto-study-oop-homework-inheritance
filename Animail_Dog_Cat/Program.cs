﻿using Animail_Dog_Cat;

Animal[] animals = new Animal[6];
animals[0] = new Dog("Dog1", 1, "Dog", "Dog1");
animals[1] = new Dog("Dog2", 2, "Dog", "Dog2");
animals[2] = new Dog("Dog3", 3, "Dog", "Dog3");
animals[3] = new Cat("Cat1", 4, "Cat", "Cat1");
animals[4] = new Cat("Cat2", 5, "Cat", "Cat2");
animals[5] = new Cat("Cat3", 6, "Cat", "Cat3");

foreach (var animal in animals)
{
    Console.WriteLine(animal.Speak());
    if (animal.Type.Equals("Dog"))
    {
        Dog.Bark();
    }
    if (animal.Type.Equals("Cat"))
    {
        Cat.Meow();
    }
}