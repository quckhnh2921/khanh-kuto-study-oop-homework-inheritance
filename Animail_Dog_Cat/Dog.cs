﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animail_Dog_Cat
{
    public class Dog : Animal
    {
        private string _breed;
        public Dog(string name, int age, string type, string breed) : base(name, age, type)
        {
            Name = name;
            Age = age;
            Type = type;
            _breed = breed;
        }

        public string Breed
        {
            get => _breed;
            set => _breed = value;
        }

        public static void Bark()
        {
            Console.WriteLine("Woof ! Woof !");
        }
    }
}
