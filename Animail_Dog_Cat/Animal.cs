﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animail_Dog_Cat
{
    public class Animal
    {
        private string _name;
        private int _age;
        private string _type;

        public Animal(string name, int age, string type)
        {
            _name = name;
            _age = age;
            _type = type;
        }

        public string Name
        {
            get => _name;
            set => _name = value;
        }

        public int Age
        {
            get => _age;
            set => _age = value;
        }

        public string Type
        {
            get => _type;
            set => _type = value;
        }

        public string Speak()
        {
            return $"The {_type} named {_name} says";
        }
    }
}
